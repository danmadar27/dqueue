// dqueue.c

#include "dqueue.h"

#ifndef MINUS_ONE
#define MINUS_ONE -1
#endif // MINUS_ONE

#ifndef ZERO
#define ZERO 0
#endif // ZERO

#ifndef ARR_LAST
#define ARR_LAST DQUEUE_SIZE - 1
#endif // ARR_LAST

void init_dqueue(dqueue_ptr dq)
{
    dq->front       = ZERO;
    dq->back        = MINUS_ONE;
    dq->total_items = ZERO;
}

void push_dqueue(dqueue_ptr dq, void * info)
{
    (dq->total_items)++;
    
    (dq->back)++;
    
    if(dq->back >= DQUEUE_SIZE)
    {
        dq->back = ZERO;
    }
    
    dq->info[dq->back] = info;
}

void * pop_front_dqueue(dqueue_ptr dq)
{
    // Variable Definitions
    
    void * removed_info;
    
    // Code Section
    
    (dq->total_items)--;
    
    removed_info = dq->info[dq->front];
    
    (dq->front)++;
    
    if(dq->front >= DQUEUE_SIZE)
    {
        dq->front = ZERO;
    }
    
    return removed_info;
}

void * pop_back_dqueue(dqueue_ptr dq)
{
    // Variable Definitions
    
    void * removed_info;
    
    // Code Section
    
    (dq->total_items)--;
    
    removed_info = dq->info[dq->back];
    
    (dq->back)--;
    
    if(dq->back < ZERO)
    {
        dq->back = ARR_LAST;
    }
    
    return removed_info;
    
}

boolean is_empty_dqueue(dqueue_ptr dq)
{
    return !dq->total_items;
}

void empty_dqueue(dqueue_ptr dq)
{
    dq->front       = ZERO;
    dq->back        = MINUS_ONE;
    dq->total_items = ZERO;
}