// dqueue.h

#ifndef _DQUEUE
#define _DQUEUE


#ifndef DQUEUE_SIZE
#define DQUEUE_SIZE 10
#endif // DQUEUE_SIZE

#ifndef _BOOL
#define _BOOL

typedef enum boolean{FALSE, TRUE} boolean;

#endif // _BOOL

//--------------------------------------------------------------------------------

typedef struct dqueue
{
	void * info[DQUEUE_SIZE]; // Holds info of items in the dqueue
	char   front;             // Stores the offset of the front of the dqueue
	char   back;              // Stores the offset of the back of the dqueue
    char   total_items;       // Stores the total items in the dqueue
}dqueue, * dqueue_ptr;


void init_dqueue(dqueue_ptr dq); // Initialize the dqueue

void push_dqueue(dqueue_ptr dq, void * info); // Push element to the back 
                                              // of the dqueue

void * pop_front_dqueue(dqueue_ptr dq); // Pops element from the front
                                        // of the queue

void * pop_back_dqueue(dqueue_ptr dq); // Pops element from the back
                                       // of the queue

boolean is_empty_dqueue(dqueue_ptr dq); // Checks if dqueue is empty

void empty_dqueue(dqueue_ptr dq); // Empty space used for the dqueue

//--------------------------------------------------------------------------------



#endif //_DQUEUE